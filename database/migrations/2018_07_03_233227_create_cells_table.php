<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCellsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cells', function (Blueprint $table) {
            $table->engine = "InnoDB"; // Needed for foreign keys to work

            $table->increments('cell_id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('users');

            $table->unsignedInteger('users_cell_id');
            $table->unique(['users_cell_id', 'user_id']);   // Avoid one user multiple cells with same id

            $table->integer('model_id')->unsigned()->nullable();
            $table->foreign('model_id')->references('model_id')->on('cell_specifications');

            $table->integer('previous_application_id')->unsigned()->nullable();
            $table->foreign('previous_application_id')->references('app_id')->on('applications');

            $table->float('start_voltage')->nullable();

            $table->timestamp('received_date')->useCurrent();

            // Updates whenever the record is modifed in anyway
            $table->timestamp('rec_last_modified')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('rec_date')->useCurrent();
        });

        // Test data
        DB::table('cells')->insert(array(
            'user_id'=>'1',  
            'users_cell_id'=>  '1',
            'model_id'=>'1',
            'previous_application_id'=>'1',
            'start_voltage'=>'1.4'
        ));

        DB::table('cells')->insert(array(
            'user_id'=>'1',  
            'users_cell_id'=>  '2',
            'model_id'=>'2',
            'previous_application_id'=>'2',
            'start_voltage'=>'1.4'
        ));

        DB::table('cells')->insert(array(
            'user_id'=>'2',  
            'users_cell_id'=>  '1',
            'model_id'=>'1',
            'previous_application_id'=>'3',
            'start_voltage'=>'0.2'
        ));

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cells');
    }
}
