<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = "InnoDB";  // Needed for foreign keys to work

            $table->increments('user_id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        // Test data
        DB::table('users')->insert(array(
                                            'name'=>'test1',  
                                            'email'=>  'test1@gmail.com',
                                            'password'=>'$2y$10$NlelgPWqmY3nDZXMUw2igOe2SS3Z0ARvRHwFxvWHxan9BGkCMPMHm', //Password = testtest1
                                        ));

        DB::table('users')->insert(array(
                                            'name'=>'test2',  
                                            'email'=>  'test2@gmail.com',
                                            'password'=>'$2y$10$d4CSkLvvCqtgk4jGzUyQtuJbv7lDDkDbxeh1xMbeARsZAvMmZ8jdG', //Password = testtest2
                                        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
