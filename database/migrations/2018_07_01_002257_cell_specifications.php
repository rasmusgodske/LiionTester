<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CellSpecifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cell_specifications', function (Blueprint $table) {
            $table->engine = "InnoDB"; // Needed for foreign keys to work
            $table->increments('model_id');

            $table->string('brand', 100);
            $table->string('model', 100);
            // Make it impossible for two rows have the same brand AND model
            $table->unique(['brand', 'model']);

            // --- Battery specifications ---
            $table->float('capacity')->nullable();
            $table->float('nominal_voltage')->nullable();
            $table->float('charge_max_voltage')->nullable();
            $table->float('charge_std_current')->nullable();
            $table->float('charge_max_current_rate')->nullable();

            $table->float('discharge_cutoff_voltage')->nullable();
            $table->float('discharge_std_current_rate')->nullable();
            $table->float('discharge_max_current_rate')->nullable();
            // -------------------------------

            $table->string('image_url')->nullable();
            $table->string('post_url')->nullable();
            $table->timestamp('post_date')->nullable();
            $table->timestamp('post_date_last_modified')->nullable();

            // Updates whenever the record is modifed in anyway
            $table->timestamp('rec_last_modified')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('rec_date')->useCurrent();
        });

        // Test data
        DB::table('cell_specifications')->insert(array(
            'brand'=>'Samsung',  
            'model'=>  'Galaxy s7',
            'capacity'=>'2300',
            'nominal_voltage'=>'3.7',
            'charge_max_voltage'=>'4.2',
            'charge_std_current'=>'1500',
            'charge_max_current_rate'=>'2000',
            'discharge_cutoff_voltage'=>'3',
            'discharge_std_current_rate'=>'3000',
            'discharge_max_current_rate'=>'4000',
            'image_url'=>'https://secondlifestorage.com/nonforum/celldata/sideimages/zxBkgah.jpg',
            'post_url'=>'https://secondlifestorage.com/t-LG-LGABF1L1865-Cell-Specifications',
            'post_date'=>'2017-07-14 15:42:00',
            'post_date_last_modified'=>'2017-07-14 15:42:00'
        ));
        DB::table('cell_specifications')->insert(array(
            'brand'=>'Sony',  
            'model'=>  'Experia S9',
            'capacity'=>'2550',
            'nominal_voltage'=>'3.7',
            'charge_max_voltage'=>'4.1',
            'charge_std_current'=>'4300',
            'charge_max_current_rate'=>'2500',
            'discharge_cutoff_voltage'=>'3',
            'discharge_std_current_rate'=>'2900',
            'discharge_max_current_rate'=>'1400',
            'image_url'=>'https://secondlifestorage.com/nonforum/celldata/sideimages/zxBkgah.jpg',
            'post_url'=>'https://secondlifestorage.com/t-LG-LGABF1L1865-Cell-Specifications',
            'post_date'=>'2017-07-14 15:42:00',
            'post_date_last_modified'=>'2017-07-14 15:42:00'
        ));


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cell_specifications');
    }
}
