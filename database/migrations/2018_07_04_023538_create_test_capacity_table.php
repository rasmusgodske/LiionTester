<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestCapacityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_capacity', function (Blueprint $table) {
            $table->engine = "InnoDB"; // Needed for foreign keys to work
            $table->increments('measurement_id');
            
            $table->integer('cell_id')->unsigned();
            $table->foreign('cell_id')->references('cell_id')->on('cells');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('users');

            $table->integer('used_test_device_id')->unsigned()->nullable();
            //$table->foreign('used_test_device_id')->references('user_id')->on('users'); <- testing_device_models table needs to be implemented to work

            $table->float('capacity');
            $table->float('start_voltage')->nullable();
            $table->float('end_voltage')->nullable();
            $table->integer('estimated_time')->unsigned()->nullable();

            $table->timestamp('measurement_date')->useCurrent();

            // Updates whenever the record is modifed in anyway
            $table->timestamp('rec_last_modified')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('rec_date')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_capacity');
    }
}
