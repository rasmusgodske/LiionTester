<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->engine = "InnoDB"; // Needed for foreign keys to work
            
            $table->increments('app_id');

            $table->string('name', 100);

            // Updates whenever the record is modifed in anyway
            $table->timestamp('rec_last_modified')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('rec_date')->useCurrent();
        });

        // Test data
        DB::table('applications')->insert(array('name'=>'Laptop Battery'));
        DB::table('applications')->insert(array('name'=>'Ebike Battery'));
        DB::table('applications')->insert(array('name'=>'Dril Battery'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
