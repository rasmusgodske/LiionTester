#!/bin/bash
PATH=$PATH:~/

git pull

composer dump-autoload -o
composer install
php artisan migrate --force
php artisan clear-compiled
php artisan view:clear
php artisan config:clear
php artisan optimize
composer dump-autoload -o