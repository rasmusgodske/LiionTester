<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cells extends Model
{
    protected $table = 'cells';
    
    protected $primaryKey = 'cell_id';
        /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    const UPDATED_AT = 'rec_last_modified';
    const CREATED_AT = 'rec_date';
}
