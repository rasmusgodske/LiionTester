<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CellSpecification extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cell_specifications';
    
    protected $primaryKey = 'model_id';

    protected $fillable = array(
                                'brand',
                                'model',
                                'capacity',
                                'nominal_voltage',
                                'charge_max_voltage',
                                'charge_std_current',
                                'charge_max_current_rate',
                                'discharge_cutoff_voltage',
                                'discharge_std_current_rate',
                                'discharge_max_current_rate',
                                'image_url',
                                'post_url',
                                'post_date',
                                'post_date_last_modified'                            
                            );

    //protected $guarded = array('model_id', 'brand', 'model');

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    const UPDATED_AT = 'rec_last_modified';
    const CREATED_AT = 'rec_date';
}
