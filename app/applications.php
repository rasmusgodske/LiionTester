<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class applications extends Model
{
    protected $table = 'applications';
    
    protected $primaryKey = 'app_id';
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    const UPDATED_AT = 'rec_last_modified';
    const CREATED_AT = 'rec_date';
}
