<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class test_internal_resistance extends Model
{
    protected $table = 'test_internal_resistance';
    
    protected $primaryKey = 'measurement_id';
        /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    const UPDATED_AT = 'rec_last_modified';
    const CREATED_AT = 'rec_date';
}
