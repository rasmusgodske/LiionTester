<?php

namespace App\Http\Controllers;

use App\CellSpecification;
use Illuminate\Http\Request;
use App\Classes\SecondLifeStorageScraper\ForumScraper;

class CellSpecificationController extends Controller
{

    public function index()
    {

    }

    public function store()
    {
        $url_root = 'https://secondlifestorage.com';
        $cellArray = $this->scrape($url_root);

        return $cellArray;
    }

    protected function scrape($url_root)
    {
        // Create a scraper object which handles all the webscraping
        $scraper = new ForumScraper($url_root);
        $cellArray = array();

        $x = 1;
        // Loop all cells on the forum
        do {
            $cell = CellSpecification::updateOrCreate(
                [
                    'brand'                         => $scraper->getBrand(),
                    'model'                         => $scraper->getModel(),
                ],
                [
                    'brand'                         => $scraper->getBrand(),
                    'model'                         => $scraper->getModel(),
                    'capacity'                      => $scraper->getCapacity(),
                    'nominal_voltage'               => $scraper->getNominalVoltage(),
                    'charge_max_voltage'            => $scraper->getChargingMaxVoltage(),
                    'charge_std_current'            => $scraper->getChargingStdCurrent(),
                    'charge_max_current_rate'       => $scraper->getChargingMaxCurrent(),
                    'discharge_cutoff_voltage'      => $scraper->getDischargingCutoffVoltage(),
                    'discharge_std_current_rate'    => $scraper->getDischargingStdCurrent(),
                    'discharge_max_current_rate'    => $scraper->getDischargingMaxCurrent(),
                    'image_url'                     => $scraper->getImageUrl(),
                    'post_date'                     => $scraper->getPostDate(),
                    'post_date_last_modified'       => $scraper->getLastModifiedDate(),
                    'post_url'                      => $scraper->getUrl()
                ]
            );
            /*
            $cell = new CellSpecification;

            $cell->brand                        = $scraper->getBrand();
            $cell->model                        = $scraper->getModel();
            $cell->capacity                     = $scraper->getCapacity();
            $cell->nominal_voltage              = $scraper->getNominalVoltage();
            $cell->charge_max_voltage           = $scraper->getChargingMaxVoltage();
            $cell->charge_std_current           = $scraper->getChargingStdCurrent();
            $cell->charge_max_current_rate      = $scraper->getChargingMaxCurrent();
            $cell->discharge_cutoff_voltage     = $scraper->getDischargingCutoffVoltage();
            $cell->discharge_std_current_rate   = $scraper->getDischargingStdCurrent();
            $cell->discharge_max_current_rate   = $scraper->getDischargingMaxCurrent();
            $cell->image_url                    = $scraper->getImageUrl();
            $cell->post_date                    = $scraper->getPostDate();
            $cell->post_date_last_modified      = $scraper->getLastModifiedDate();
            $cell->post_url                     = $scraper->getUrl();


            $cell->save();
            */
            array_push($cellArray, $cell);
            $x++;
            if ($x > 300) break;
        } while ($scraper->nextCell());

        //return $cellArray;
        return view('scanner')->with('cells', $cellArray);
    }

}
