<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class test_self_discarge extends Model
{
    protected $table = 'test_self_discharge';
    
    protected $primaryKey = 'measurement_id';
        /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    const UPDATED_AT = 'rec_last_modified';
    const CREATED_AT = 'rec_date';
}
