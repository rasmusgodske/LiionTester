<?php
namespace App\Classes\SecondLifeStorageScraper;
use DateTime;
ini_set('max_execution_time', 600); //600 seconds = 10 minutes

require('simple_html_dom.php');
class ForumScraper {
    private $subForum = "/f-Cell-Data-Specifications";

    private $url;						// URL of the Cell Data & Specifications page

    private $numberOfPages = 0;			// Number of pages found
    private $curentPageNumber = 0;		// The current page being scraped

    private $threadLinks = array();
    
    private $currentCellUrl;
    private $html;

    private $defaultValue = NULL;

    public function __construct($_url)
    {
        $this->url = $_url;

        // Get the total number of pages found on Cell Data & Specifications
        $this->numberOfPages = $this->getNumberOfPages($_url . $this->subForum);
        $this->nextCell();
    }

    public function nextCell()
    {
        //echo "NextCell()";
        if (count($this->threadLinks) == 0)
        {
            //echo "No threadLinks left<br>";
            if ($this->curentPageNumber < $this->numberOfPages)
            {
                //echo "Scanning for more threads";
                $this->curentPageNumber++;
                $this->getThreadLinks();	// Update the threadLinks from the next page
                $nextTheadLink = array_shift($this->threadLinks);
            }
            else
            {
                //echo "No more cells left to scan <br>";
                return false;	// No more cells to scan
            }
        }

        $nextTheadLink = array_shift($this->threadLinks);
        //echo "Next thread link: " . $nextTheadLink . "<br>";

        // Grab the HTML for next cell thread
        $this->html = file_get_html( $nextTheadLink );
        $this->currentCellUrl = $nextTheadLink;

        return true;
    }

    public function getUrl()
    {
        return $this->currentCellUrl;
    }

    public function getBrand()
    {
        $brand = $this->html->find('table.celldata tbody',0)->children(1)->children(1)->plaintext;
        return $brand;
    }

    public function getModel()
    {
        $model = $this->html->find('table.celldata tbody',0)->children(2)->children(1)->plaintext;
        return $model;
    }

    public function getCapacity()
    {
        $table = $this->html->find('table.celldata tbody',0);
        $capacity = $table->children(3)->children(1)->plaintext;
        $capacity = str_replace("mAh Rated", "", $capacity);	// Remove addtional text
        
        // The forum uses "--" when no specification is found, next line handles that
        if (!is_numeric($capacity)) $capacity = $this->defaultValue;
        return $capacity;
    }

    public function getNominalVoltage()
    {
        $table = $this->html->find('table.celldata tbody',0);
        $nominal_voltage = $table->children(4)->children(1)->plaintext;
        $nominal_voltage = str_replace("V Nominal", "", $nominal_voltage); // Remove addtional text
        // The forum uses "--" when no specification is found, next line handles that
        if (!is_numeric($nominal_voltage)) $nominal_voltage = $this->defaultValue;
        return $nominal_voltage;
    }

    public function getChargingMaxVoltage()
    {
        $table = $this->html->find('table.celldata tbody',0);
        // Get chargin stats
        $charging = $table->children(5)->children(1)->plaintext;		// Get the charging stats		
        
        $arr = explode("\n", $charging);		   					 // Split $charging and save each result in above three variables
        $charging_max_V = $arr[0];
        $charging_max_V = str_replace("V Maximum\r", "", $charging_max_V);				// Remove aditional text: 'V Maximum\r'
        // The forum uses "--" when no specification is found, next line handles that
        if (!is_numeric($charging_max_V)) $charging_max_V = $this->defaultValue;
        return $charging_max_V;
    }

    public function getChargingStdCurrent()
    {
        $table = $this->html->find('table.celldata tbody',0);
        // Get chargin stats
        $charging = $table->children(5)->children(1)->plaintext;		// Get the charging stats		
        $arr = explode("\n", $charging);		   					 // Split $charging and save each result in above three variables
        $charging_stand_mA = $arr[1];
        
        $charging_stand_mA = str_replace("mA Standard\r", "", $charging_stand_mA);				// Remove aditional text: 'V Maximum\r'

        // The forum uses "--" when no specification is found, next line handles that
        if (!is_numeric($charging_stand_mA)) $charging_stand_mA = $this->defaultValue;
        return $charging_stand_mA;
    }

    public function getChargingMaxCurrent()
    {
        $table = $this->html->find('table.celldata tbody',0);

        // Get chargin stats
        $charging = $table->children(5)->children(1)->plaintext;		// Get the charging stats		
        
        $arr = explode("\n", $charging);		   					 // Split $charging and save each result in above three variables
        $charging_max_mA = $arr[2];

        $charging_max_mA = str_replace("mA Maximum", "", $charging_max_mA);	
        
        // The forum uses "--" when no specification is found, next line handles that
        if (!is_numeric($charging_max_mA)) $charging_max_mA = $this->defaultValue;
        return $charging_max_mA;		
    }

    public function getDischargingCutoffVoltage()
    {
        $table = $this->html->find('table.celldata tbody',0);

        // Get chargin stats
        $discharging = $table->children(6)->children(1)->plaintext;		// Get the discharging stats		
        
        $arr = explode("\n", $discharging);		   					 // Split $charging and save each result in above three variables
        $discharging_cutoff = $arr[0];
        
        $discharging_cutoff = str_replace("V Cutoff\r", "", $discharging_cutoff);				// Remove aditional text: 'V Maximum\r'

        // The forum uses "--" when no specification is found, next line handles that
        if (!is_numeric($discharging_cutoff)) $discharging_cutoff = $this->defaultValue;
        return $discharging_cutoff;
    }


    public function getDischargingStdCurrent()
    {
        $table = $this->html->find('table.celldata tbody',0);

        // Get chargin stats
        $discharging = $table->children(6)->children(1)->plaintext;		// Get the discharging stats		
        
        $arr = explode("\n", $discharging);		   					 // Split $charging and save each result in above three variables
        $discharging_std_mA = $arr[1];
        
        $discharging_std_mA = str_replace("mA Standard\r", "", $discharging_std_mA);				// Remove aditional text: 'V Maximum\r'
        // The forum uses "--" when no specification is found, next line handles that
        if (!is_numeric($discharging_std_mA)) $discharging_std_mA = $this->defaultValue;
        return $discharging_std_mA;		
    }


    public function getDischargingMaxCurrent()
    {
        $table = $this->html->find('table.celldata tbody',0);

        // Get chargin stats
        $discharging = $table->children(6)->children(1)->plaintext;		// Get the discharging stats		
        
        $arr = explode("\n", $discharging);		   					 // Split $charging and save each result in above three variables
        $discharging_max_mA = $arr[2];
        
        $discharging_max_mA = str_replace("mA Maximum", "", $discharging_max_mA);				// Remove aditional text: 'V Maximum\r'
        if (!is_numeric($discharging_max_mA)) $discharging_max_mA = $this->defaultValue;
        return $discharging_max_mA;		
    }

    public function getPostDate()
    {
        $returnDateFormat = 'Y-m-d G:i:s';

        // Get the date when this post where made
        $post_date = $this->html->find('span.post_date',0)->plaintext;
        
        // Get the date the post was last modified if it was
        $post_date_last_modified = $this->html->find('span.post_date',0)->children(0)->plaintext;
        
        if ($post_date_last_modified != " ") // If modified date was provided
        {
            // $post_date will both have the post date, but also the last modifed date
            // So we have to remove the last modifed date from the post date
            $post_date = str_replace($post_date_last_modified, "", $post_date);	
        
            $post_date_last_modified = str_replace("(This post was last modified: ", "", $post_date_last_modified);	// Remove aditional text: '(This post was last modified: '
            $post_date_last_modified = current(explode(' by ', $post_date_last_modified));;							 // Remove aditional text: ' by  ' and everything after that
        }
                    
        // Convert post_date to a known mysql Timestamp format
        // For when inserting it into mysql database
        $dateFormat = 'm-d-Y, h:i a+';	// Format website uses
        $date = DateTime::createFromFormat($dateFormat, $post_date);
        $post_date = $date->format($returnDateFormat);
        
        return $post_date;
    }

    public function getLastModifiedDate()
    {
        $returnDateFormat = 'Y-m-d G:i:s';

        // Get the date the post was last modified if it was
        $post_date_last_modified = $this->html->find('span.post_date',0)->children(0)->plaintext;
        
        if ($post_date_last_modified != " ") // If modified date was provided
        {
            $post_date_last_modified = str_replace("(This post was last modified: ", "", $post_date_last_modified);	// Remove aditional text: '(This post was last modified: '
            $post_date_last_modified = current(explode(' by ', $post_date_last_modified));;							// Remove aditional text: ' by  ' and everything after that
            
            // Convert to a known mysql Timestamp format
            $dateFormat = ' m-d-Y, h:i a+';	// The date format secondlifestorage uses
            $date = DateTime::createFromFormat($dateFormat, $post_date_last_modified);	// Convert date into a date object
            //print_r(DateTime::getLastErrors());										// Uncommet to print any erros happend when converting date
            $post_date_last_modified = $date->format($returnDateFormat);				// Save to a mysql timestamp
        }
        else
        {
            $post_date_last_modified = $this->getPostDate();	// Use postDate as defualt if post haven't been modified
        }

        return $post_date_last_modified;
    }

    public function getImageUrl()
    {
        $table = $this->html->find('table.celldata tbody',0);
        $imageSrc = $table->children(0)->find('img',0)->src;
        return $imageSrc;
    }

    private function getNumberOfPages($_url)
    {
        $html = file_get_html($_url);									// Request all HTML of the Cell data page
        $page_number = $html->find('a.pagination_last',0)->innertext;	// Get the number of pages
        if (!is_numeric($page_number)) return false;					// Check for invalid pagenumber
        return $page_number;
    }

    private function getThreadLinks()
    {
        //echo "Scanning new threadLinks<br>";

        // Get html from current forum page
        $html = file_get_html($this->url . $this->subForum . '?page=' . $this->curentPageNumber);

        // Loop all found <a> tags
        foreach($html->find('tr.inline_row td.forumdisplay_regular span a') as $a) 
        {
            // Only use links pointing to cell specification threads
            if (strpos($a->href, 'Cell-Specifications') !== false) 
            {
                // Remove all GET parameters 
                $url = explode("?", $this->url . "/" . $a->href);
                $url = $url[0];

                // Avoid dublicates
                if (!in_array($url, $this->threadLinks))
                {
                    $this->threadLinks[] = $url;
                }
            }
        }
    } 
}
?>