@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if(count($cells)>0)
                        <table class="table table-striped">
                            <tr>
                                <th>Brand</th>
                                <th>Model</th>
                                <th>Capacity</th>
                                <th>Nominal V</th>
                                <th>C.Max V</th>
                                <th>C.STD mA</th>
                                <th>C.Max mA</th>
                                <th>D.Cutoff</th>
                                <th>D.STD mA</th>
                                <th>D.Max mA</th>
                                <th>Link</th>
                                <th>Url</th>
                            </tr>
                            @foreach($cells as $cell)
                                <tr>
                                    <td>{{$cell->brand}}</td>
                                    <td>{{$cell->model}}</td>
                                    <td>{{$cell->capacity}}</td>
                                    <td>{{$cell->nominal_voltage}}</td>
                                    <td>{{$cell->charge_max_voltage}}</td>
                                    <td>{{$cell->charge_std_current}}</td>
                                    <td>{{$cell->charge_max_current_rate}}</td>
                                    <td>{{$cell->discharge_cutoff_voltage}}</td>
                                    <td>{{$cell->discharge_std_current_rate}}</td>
                                    <td>{{$cell->discharge_max_current_rate}}</td>
                                    <td><a href="{{$cell->post_url}}">Link</a></td>
                                    <td><img src="{{$cell->image_url}}" height="40" width="auto"></td>
                                </tr> 
                            @endforeach
                        </table>
                    @else
                        <p>You Have no posts</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
