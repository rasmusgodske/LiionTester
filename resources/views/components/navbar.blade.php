@push('styles')
    <link href="{{ asset('css/dashboard_navbar.css') }}" rel="stylesheet">
@endpush

<div class="dashboard_navbar">
  <div class="title">Dashboard</div>
  <a class="selected" href="#">My stats</a>
  <a href="#">Cell List</a>
  <a href="#">Settings</a>

  <div class="title">Cell Management</div>
  <a href="#">Register Cell</a>
  <a href="#">New IR Test</a>
  <a href="#">New Capacity Test</a>
  <a href="#">New Voltage Drop Test</a>
</div>

